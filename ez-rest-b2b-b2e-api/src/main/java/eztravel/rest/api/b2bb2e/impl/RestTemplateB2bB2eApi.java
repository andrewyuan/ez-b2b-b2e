/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.api.payment.impl
 * @FileName: RestTemplatePaymentApi.java
 * @author:   ralflin
 * @date:     2014/8/26, 上午 11:01:11
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.api.b2bb2e.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


import tw.com.eztravel.ezlogger.Logger;
import tw.com.eztravel.ezlogger.LoggerFactory;
import eztravel.rest.api.b2bb2e.B2bB2eApi;
import eztravel.rest.pojo.b2bb2e.PriceInfo;
import eztravel.rest.pojo.b2e.B2EInfo;
import eztravel.rest.pojo.b2e.GrantsB2eContactInfo;
import eztravel.rest.pojo.b2e.GrantsEmployeeGrantInfo;
import eztravel.rest.pojo.b2e.GrantsEmployeeInfo;
import eztravel.rest.pojo.b2e.GrantsInfo;
import eztravel.rest.pojo.b2e.GrantsTarget;
import eztravel.rest.pojo.common.RestError;
import eztravel.rest.pojo.common.RestResource;


/**
 * <pre>
 * Payment API，Spring RestTemplate實作版.
 * </pre>
 * 
 * @author Ralf
 */
public class RestTemplateB2bB2eApi implements B2bB2eApi {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(RestTemplateB2bB2eApi.class);

	/**
	 * 企業助補款對象.
	 * 
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public List<GrantsTarget> listGrantsTarget() throws Exception {
		List<GrantsTarget> grantTargetList = new ArrayList<GrantsTarget>();

		ParameterizedTypeReference<RestResource<GrantsTarget>> responseType = new ParameterizedTypeReference<RestResource<GrantsTarget>>() {
		};
		Map<String, Object> uriVariables = new HashMap<String, Object>();

		ResponseEntity<RestResource<GrantsTarget>> response = restTemplate
				.exchange(endpoint + LIST_GRANTS_TARGET, HttpMethod.GET, null,
						responseType, uriVariables);

		RestResource<GrantsTarget> body = response.getBody();

		if (null != body) {
			List<RestError> errors = body.getErrors();
			if (null != errors) {
				for (RestError error : errors)
					logger.error(error.getMessage(), error);
			}
			grantTargetList = body.getItems();
		}
		return grantTargetList;
	}

	/**
	 * 取得補助款對象的所屬B2E企業的補助款設定值.
	 * 
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public List<GrantsInfo> getGrantsSetting(String dealerNo) throws Exception {

		List<GrantsInfo> grantsSettingList = new ArrayList<GrantsInfo>();
		ParameterizedTypeReference<RestResource<GrantsInfo>> responseType = new ParameterizedTypeReference<RestResource<GrantsInfo>>() {
		};
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		uriVariables.put("dealerNo", dealerNo);

		ResponseEntity<RestResource<GrantsInfo>> response = restTemplate
				.exchange(endpoint + GET_GRANTS_SETTING, HttpMethod.GET, null,
						responseType, uriVariables);

		RestResource<GrantsInfo> body = response.getBody();

		if (null != body) {
			List<RestError> errors = body.getErrors();
			if (null != errors) {
				for (RestError error : errors)
					logger.error(error.getMessage(), error);
			}
			grantsSettingList = body.getItems();
		}
		return grantsSettingList;
	}

	/**
	 * 企業會員所擁有的部門.
	 * 
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public List<String> listGrantsDepartment(String dealerNo) throws Exception {
		List<String> departmentList = new ArrayList<String>();
		ParameterizedTypeReference<RestResource<String>> responseType = new ParameterizedTypeReference<RestResource<String>>() {
		};
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		uriVariables.put("dealerNo", dealerNo);

		ResponseEntity<RestResource<String>> response = restTemplate.exchange(
				endpoint + LIST_GRANTS_DEPARTMENT, HttpMethod.GET, null,
				responseType, uriVariables);

		RestResource<String> body = response.getBody();

		if (null != body) {
			List<RestError> errors = body.getErrors();
			if (null != errors) {
				for (RestError error : errors)
					logger.error(error.getMessage(), error);
			}
			departmentList = body.getItems();
		}
		return departmentList;
	}

	/**
	 * 驗證是否有企業員工名冊資料.
	 * 
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public List<GrantsEmployeeInfo> listEmployee(String dealerNo)
			throws Exception {

		List<GrantsEmployeeInfo> grantsInfoExistList = new ArrayList<GrantsEmployeeInfo>();

		ParameterizedTypeReference<RestResource<GrantsEmployeeInfo>> responseType = new ParameterizedTypeReference<RestResource<GrantsEmployeeInfo>>() {
		};
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		uriVariables.put("dealerNo", dealerNo);

		ResponseEntity<RestResource<GrantsEmployeeInfo>> response = restTemplate
				.exchange(endpoint + LIST_EMPLOYEE, HttpMethod.GET, null,
						responseType, uriVariables);

		RestResource<GrantsEmployeeInfo> body = response.getBody();

		if (null != body) {
			List<RestError> errors = body.getErrors();
			if (null != errors) {
				for (RestError error : errors)
					logger.error(error.getMessage(), error);
			}
			grantsInfoExistList = body.getItems();
		}
		return grantsInfoExistList;
	}

	@Override
	public List<GrantsEmployeeGrantInfo> getEmployeeGrantInfo(String dealerNo,
			String empId) throws Exception {

		List<GrantsEmployeeGrantInfo> grantsInfoByAmtList = new ArrayList<GrantsEmployeeGrantInfo>();

		ParameterizedTypeReference<RestResource<GrantsEmployeeGrantInfo>> responseType = new ParameterizedTypeReference<RestResource<GrantsEmployeeGrantInfo>>() {
		};
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		uriVariables.put("dealerNo", dealerNo);
		uriVariables.put("empId", empId);

		ResponseEntity<RestResource<GrantsEmployeeGrantInfo>> response = restTemplate
				.exchange(endpoint + GET_EMPLOYEE_GRANT_INFO, HttpMethod.GET,
						null, responseType, uriVariables);

		RestResource<GrantsEmployeeGrantInfo> body = response.getBody();

		if (null != body) {
			List<RestError> errors = body.getErrors();
			if (null != errors) {
				for (RestError error : errors)
					logger.error(error.getMessage(), error);
			}
			grantsInfoByAmtList = body.getItems();
		}
		return grantsInfoByAmtList;
	}

	@Override
	public List<GrantsEmployeeInfo> listEmployeeByName(String dealerNo,
			String empName) throws Exception {

		List<GrantsEmployeeInfo> getEmployeeByName = new ArrayList<GrantsEmployeeInfo>();

		ParameterizedTypeReference<RestResource<GrantsEmployeeInfo>> responseType = new ParameterizedTypeReference<RestResource<GrantsEmployeeInfo>>() {
		};
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		uriVariables.put("dealerNo", dealerNo);
		uriVariables.put("name", java.net.URLEncoder.encode(empName, "UTF-8"));

		ResponseEntity<RestResource<GrantsEmployeeInfo>> response = restTemplate
				.exchange(endpoint + LIST_EMPLOYEE_BY_NAME, HttpMethod.GET,
						null, responseType, uriVariables);

		RestResource<GrantsEmployeeInfo> body = response.getBody();

		if (null != body) {
			List<RestError> errors = body.getErrors();
			if (null != errors) {
				for (RestError error : errors)
					logger.error(error.getMessage(), error);
			}
			getEmployeeByName = body.getItems();
		}
		return getEmployeeByName;
	}

	@Override
	public List<GrantsEmployeeInfo> getEmployee(String dealerNo, String empId)
			throws Exception {

		List<GrantsEmployeeInfo> getEmployeeById = new ArrayList<GrantsEmployeeInfo>();
		ParameterizedTypeReference<RestResource<GrantsEmployeeInfo>> responseType = new ParameterizedTypeReference<RestResource<GrantsEmployeeInfo>>() {
		};
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		uriVariables.put("dealerNo", dealerNo);
		uriVariables.put("empId", empId);

		ResponseEntity<RestResource<GrantsEmployeeInfo>> response = restTemplate
				.exchange(endpoint + GET_EMPLOYEE, HttpMethod.GET, null,
						responseType, uriVariables);

		RestResource<GrantsEmployeeInfo> body = response.getBody();
		if (null != body) {
			List<RestError> errors = body.getErrors();
			if (null != errors) {
				for (RestError error : errors)
					logger.error(error.getMessage(), error);
			}
			getEmployeeById = body.getItems();
		}
		return getEmployeeById;
	}

	@Override
	public List<GrantsB2eContactInfo> b2eContactList(String dealerNo)
			throws Exception {

		List<GrantsB2eContactInfo> getEmployeeById = new ArrayList<GrantsB2eContactInfo>();

		ParameterizedTypeReference<RestResource<GrantsB2eContactInfo>> responseType = new ParameterizedTypeReference<RestResource<GrantsB2eContactInfo>>() {
		};
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		uriVariables.put("dealerNo", dealerNo);

		ResponseEntity<RestResource<GrantsB2eContactInfo>> response = restTemplate
				.exchange(endpoint + GET_B2E_CONTACT_LIST, HttpMethod.GET,
						null, responseType, uriVariables);

		RestResource<GrantsB2eContactInfo> body = response.getBody();

		if (null != body) {
			List<RestError> errors = body.getErrors();
			if (null != errors) {
				for (RestError error : errors)
					logger.error(error.getMessage(), error);
			}
			getEmployeeById = body.getItems();
		}
		return getEmployeeById;
	}


	  @Override
	  public List<PriceInfo> queryRolePriceInfo(String role, String b2cPrice, String prodNo,
	      String personId) throws Exception {

	    List<PriceInfo> part = new ArrayList<PriceInfo>();
	    ParameterizedTypeReference<RestResource<PriceInfo>> responseType =
	            new ParameterizedTypeReference<RestResource<PriceInfo>>() {};
	            
	        Map<String, Object> uriVariables = new HashMap<String, Object>();
	        uriVariables.put("businessType", role);
	        uriVariables.put("price", b2cPrice);
	        uriVariables.put("prodNo", prodNo);
	        uriVariables.put("loginID", personId);

	        ResponseEntity<RestResource<PriceInfo>> response =
	            restTemplate.exchange(endpoint + QUERY_ROLE_PRICE_INFO, HttpMethod.GET, null, responseType, uriVariables);

	        RestResource<PriceInfo> body = response.getBody();
	        if (null != body) {
				List<RestError> errors = body.getErrors();
				if (null != errors) {
					for (RestError error : errors)
						logger.error(error.getMessage(), error);
				}
				part = body.getItems();
			}
	        
	    return part;
	  }
	  
	  @Override
	  public String queryb2eImg(String personId) throws Exception {
	    
	    List<B2EInfo> b2eImg = new ArrayList<B2EInfo>();

	    	    ParameterizedTypeReference<RestResource<B2EInfo>> responseType =
	    	        new ParameterizedTypeReference<RestResource<B2EInfo>>() {};

	    	    Map<String, Object> uriVariables = new HashMap<String, Object>();
	    	    uriVariables.put("personId", personId);

	    	    ResponseEntity<RestResource<B2EInfo>> response =
	    	        restTemplate.exchange(endpoint + QUERY_B2E_IMG, HttpMethod.GET, null, responseType, uriVariables);

	    	    RestResource<B2EInfo> body = response.getBody();
	    	    
	    	    if (null != body) {
					List<RestError> errors = body.getErrors();
					if (null != errors) {
						for (RestError error : errors)
							logger.error(error.getMessage(), error);
					}
					b2eImg = body.getItems();
				}
	    		
	    if (b2eImg != null && b2eImg.size() >= 1) {
	      return b2eImg.get(0).getSubsidyMsg();
	    } else {
	      return null;
	    }
	  }
	  
	/**
	 * Instantiates a new rest template payment api.
	 * 
	 * @param restTemplate
	 *            the rest template
	 */
	public RestTemplateB2bB2eApi(RestTemplate restTemplate) {
		InputStream is = null;
		this.restTemplate = restTemplate;
		try {
			Properties properties = new Properties();
			is = RestTemplateB2bB2eApi.class.getClassLoader()
					.getResourceAsStream("b2b-b2e-api-config-test.properties");
			if (null == is) {
				is = RestTemplateB2bB2eApi.class.getClassLoader()
						.getResourceAsStream("b2b-b2e-api-config.properties");
			}
			properties.load(is);
			this.endpoint = properties.getProperty("endpoint");
		} catch (Exception e) {
			logger.warn(e.getMessage(), e);
		} finally {
			try {
				if (null != is)
					is.close();
			} catch (IOException e) {
				logger.warn(e.getMessage(), e);
			}
		}
	}

	/** The rest template. */
	private RestTemplate restTemplate;

	/** The endpoint. */
	private String endpoint;

	/**
	 * Gets the rest template.
	 * 
	 * @return the restTemplate
	 */
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	/**
	 * Sets the rest template.
	 * 
	 * @param restTemplate
	 *            the restTemplate to set
	 */
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	/**
	 * Gets the endpoint.
	 * 
	 * @return the endpoint
	 */
	public String getEndpoint() {
		return endpoint;
	}

	/**
	 * Sets the endpoint.
	 * 
	 * @param endpoint
	 *            the endpoint to set
	 */
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}


}
