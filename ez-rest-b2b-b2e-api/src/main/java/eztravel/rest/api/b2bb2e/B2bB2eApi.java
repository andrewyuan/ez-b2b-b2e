/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.api.payment
 * @FileName: B2bB2eAPI.java
 * @author:   treylin
 * @date:     2014/9/16, 上午 10:31:22
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.api.b2bb2e;

import java.util.List;

import eztravel.rest.pojo.b2bb2e.PriceInfo;
import eztravel.rest.pojo.b2e.GrantsB2eContactInfo;
import eztravel.rest.pojo.b2e.GrantsEmployeeGrantInfo;
import eztravel.rest.pojo.b2e.GrantsEmployeeInfo;
import eztravel.rest.pojo.b2e.GrantsInfo;
import eztravel.rest.pojo.b2e.GrantsTarget;

/**
 * <pre>
 * B2b B2e Service API.
 * </pre>
 * 
 * @author Trey Lin
 */
public interface B2bB2eApi {

	/** 企業補助款對象 */
	public static final String LIST_GRANTS_TARGET = "grants/target";
	/** 企業補助款相關設定資訊 */
	public static final String GET_GRANTS_SETTING = "grants/{dealerNo}";
	/** 企業會員所擁有的部門 */
	public static final String LIST_GRANTS_DEPARTMENT = "grants/{dealerNo}/department";
	/** 企業會員所擁有的員工列表 */
	public static final String LIST_EMPLOYEE = "grants/{dealerNo}/employee";
	/** 取得企業員工依員工編號查詢 */
	public static final String GET_EMPLOYEE = "grants/{dealerNo}/employee/{empId}";
	/** 取得企業員工依姓名查詢 */
	public static final String LIST_EMPLOYEE_BY_NAME = "grants/{dealerNo}/employee?name={name}";
	/** 取得員工補助金額度、已使用金額 */
	public static final String GET_EMPLOYEE_GRANT_INFO = "grants/{dealerNo}/employee/{empId}/grant";
	/** 取得企業的補助款福委窗口 */
	public static final String GET_B2E_CONTACT_LIST = "grants/{dealerNo}/b2eContact";
	
	/** 查詢b2b/b2e售價 */
	public static final String QUERY_ROLE_PRICE_INFO = "price/{businessType}/{price}/{prodNo}/{loginID}";

	/** 查詢B2E付款流程圖URL */
	public static final String QUERY_B2E_IMG ="b2einfo/{personId}";
	
	/**
   * 補助對象.
   * 
   * @return the list
   * @throws Exception the exception
   */
  public List<GrantsTarget> listGrantsTarget() throws Exception;
  
  /**
   * 取得補助款對象的所屬B2E企業的補助款設定值.
   * 
   * @return the list
   * @throws Exception the exception
   */
  public List<GrantsInfo> getGrantsSetting(String dealNo) throws Exception;

  /**
   * 企業會員所擁有的部門.
   * 
   * @return the list
   * @throws Exception the exception
   */
  public List<String> listGrantsDepartment(String dealNo) throws Exception;
  
  /**
   * 驗證是否有企業員工名冊資料.
   * 
   * @return the list
   * @throws Exception the exception
   */
  public List<GrantsEmployeeInfo> listEmployee(String dealNo) throws Exception;
  
  /**
   * 補助款-企業員工額度，已使用額度
   * 
   * @return the list
   * @throws Exception the exception
   */
  public List<GrantsEmployeeGrantInfo>  getEmployeeGrantInfo(String dealNo, String empId) throws Exception;
  
  
  /**
   * 補助款-驗證客戶補助款額度 (企業員工依姓名查詢)
   * 
   * @return the list
   * @throws Exception the exception
   */
  public List<GrantsEmployeeInfo>  listEmployeeByName(String dealNo, String empName) throws Exception;
  
  
  /**
   * 補助款-企業員工依員工編號查詢
   * 
   * @return the list
   * @throws Exception the exception
   */
  public List<GrantsEmployeeInfo>  getEmployee(String dealNo, String empId) throws Exception;

  
  /**
   * 補助款-取得企業的補助款福委窗口
   * 
   * @return the list
   * @throws Exception the exception
   */
  public List<GrantsB2eContactInfo> b2eContactList(String dealerNo) throws Exception;
  
  
  /**
   * 查詢b2b/b2e售價.
   * 
   * @param role the role
   * @param b2cPrice the b2c price
   * @param prodNo the prod no
   * @param personId the person id
   * @return the list
   * @throws Exception the exception
   */
  public List<PriceInfo> queryRolePriceInfo(String role, String b2cPrice, String prodNo,
      String personId) throws Exception;
  
  /**
   * 查詢B2E付款流程圖URL.
   * 
   * @param personId the person id
   * @return the string
   * @throws Exception the exception
   */
  public String queryb2eImg(String personId) throws Exception;
}
