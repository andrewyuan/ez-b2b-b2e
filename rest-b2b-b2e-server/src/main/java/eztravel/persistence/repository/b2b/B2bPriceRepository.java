/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.b2b
 * @FileName: B2bPriceRepository.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.b2b;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import eztravel.rest.pojo.b2bb2e.QueryB2bB2ePriceResult;


/**
 * The Interface B2bPriceRepository.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface B2bPriceRepository {

  // 查詢 B2B、B2E 狀態
  /**
   * Query b2b b2e yn.
   * 
   * @param prodNo the prod no
   * @return the query b2b b2e price result
   */
  QueryB2bB2ePriceResult queryB2bB2eYN(@Param("prodNo") String prodNo);
  
  // 查詢 B2B、B2E 狀態(通用全線商品）(適合擁有"合格"的:1.上架日+下架日 2.上架日+null 3.null+下架日 4.上下架日皆沒有)
  /**
   * Query b2b b2e yn.
   * 
   * @param prodNo the prod no
   * @return the query b2b b2e price result
   */
  QueryB2bB2ePriceResult generalQueryB2bB2eYN(@Param("prodNo") String prodNo);

  // 針對副類別為ODT的商品，判斷是國內旅遊還是高鐵或環島之星的商品
  /**
   * Query prod travel type.
   * 
   * @param prodNo the prod no
   * @return the query b2b b2e price result
   */
  QueryB2bB2ePriceResult queryProdTravelType(@Param("prodNo") String prodNo);

  // 查詢高鐵供應商設定的佣金值
  /**
   * Query hsr company commission.
   * 
   * @param prodNo the prod no
   * @return the query b2b b2e price result
   */
  QueryB2bB2ePriceResult queryHSRCompanyCommission(@Param("prodNo") String prodNo);

  // 查詢B2B同業佣金設定
  /**
   * Query b2 b commission.
   * 
   * @return the list
   */
  List<QueryB2bB2ePriceResult> queryB2BCommission();

  // 檢核國內旅遊商品是否為「離島」的商品
  /**
   * Check internal grp for insland prod.
   * 
   * @param prodNo the prod no
   * @return the query b2b b2e price result
   */
  QueryB2bB2ePriceResult checkInternalGRPForInslandProd(@Param("prodNo") String prodNo);

  // 檢核國內旅遊商品是否為「高鐵假期組」的商品
  /**
   * Check internal grp for h05 prod.
   * 
   * @param prodNo the prod no
   * @return the query b2b b2e price result
   */
  QueryB2bB2ePriceResult checkInternalGRPForH05Prod(@Param("prodNo") String prodNo);

  // 是否為B2B商品
  /**
   * Checks if is b2 b prod.
   * 
   * @param prodNo the prod no
   * @return the query b2b b2e price result
   */
  QueryB2bB2ePriceResult isB2BProd(@Param("prodNo") String prodNo);
}
