/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.b2e
 * @FileName: B2eInfoRepository.java
 * @author:   treylin
 * @date:     2013/9/16, 下午 12:47:16
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.b2e;

import java.util.List;

import eztravel.rest.pojo.b2e.B2EInfo;



/**
 * The Interface B2eInfoRepository.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface B2eInfoRepository {

	  // 功能: 查詢B2E付款流程圖URL
	  /**
	   * Query b2e pay flow chart info.
	   * 
	   * @param personId the person id
	   * @return the list
	   */
	  List<B2EInfo> queryB2EPayFlowChartInfo(String personId);
}
