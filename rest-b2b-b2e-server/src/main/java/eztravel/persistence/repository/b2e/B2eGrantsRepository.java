/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.member
 * @FileName: B2eGrantsRepository.java
 * @author:   Kent
 * @date:     2014/2/11, 下午 02:18:48
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.b2e;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import eztravel.rest.pojo.CodeDetail;
import eztravel.rest.pojo.b2e.B2eContact;
import eztravel.rest.pojo.b2e.B2eEmployee;
import eztravel.rest.pojo.b2e.B2eEmployeeGrant;
import eztravel.rest.pojo.b2e.GrantsSetting;

/**
 * The Interface B2eGrantsRepository.
 * 
 * <pre>
 * 企業補助款.
 * </pre>
 * 
 * @author Kent
 */
public interface B2eGrantsRepository {

  /**
   * List grants to.
   * 
   * @return the list
   */
  public List<CodeDetail> listGrantsTarget();

  /**
   * Gets the grants setting.
   * 
   * @param dealerNo the dealer no
   * @return the grants setting
   */
  public GrantsSetting getGrantsSetting(@Param("dealerNo") String dealerNo);

  /**
   * List department.
   * 
   * @param dealerNo the dealer no
   * @return the list
   */
  public List<String> listDepartment(@Param("dealerNo") String dealerNo);

  /**
   * List employee.
   * 
   * @param dealerNo the dealer no
   * @return the list
   */
  public List<B2eEmployee> listEmployee(@Param("dealerNo") String dealerNo);

  /**
   * List employee by name.
   * 
   * @param dealerNo the dealer no
   * @param empName the emp name
   * @return the list
   */
  public List<B2eEmployee> listEmployeeByName(@Param("dealerNo") String dealerNo,
      @Param("empName") String empName);

  /**
   * Gets the employee.
   * 
   * @param dealerNo the dealer no
   * @param empId the emp id
   * @return the employee
   */
  public B2eEmployee getEmployee(@Param("dealerNo") String dealerNo, @Param("empId") String empId);

  /**
   * Gets the employee grant.
   * 
   * @param dealerNo the dealer no
   * @param empId the emp id
   * @return the employee grant
   */
  public B2eEmployeeGrant getEmployeeGrant(@Param("dealerNo") String dealerNo,
      @Param("empId") String empId);

  /**
   * Gets the employee grant used.
   * 
   * @param dealerNo the dealer no
   * @param empId the emp id
   * @return the employee grant used
   */
  public BigDecimal getEmployeeGrantUsed(@Param("dealerNo") String dealerNo,
      @Param("empId") String empId);
  
  /**
   * Gets the b2e contact.
   * 
   * @param dealerNo the dealer no
   * @return the b2e contact
   */
  public B2eContact getB2eContact(@Param("dealerNo") String dealerNo);
  
}
