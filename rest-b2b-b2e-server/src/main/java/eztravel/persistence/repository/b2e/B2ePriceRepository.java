/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.b2e
 * @FileName: B2ePriceRepository.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:16
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.b2e;

import org.apache.ibatis.annotations.Param;

import eztravel.rest.pojo.b2bb2e.QueryB2bB2ePriceResult;


/**
 * The Interface B2ePriceRepository.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface B2ePriceRepository {

  // 檢核custNo是否為有效的B2E會員
  /**
   * Checks if is available dealer no.
   * 
   * @param loginID the login id
   * @return the query b2b b2e price result
   */
  QueryB2bB2ePriceResult isAvailableDealerNo(@Param("loginID") String loginID);

  // 查詢 TBLB2E_COMMISSION_PROD 中的折扣值
  /**
   * Gets the product commission.
   * 
   * @param personId the person id
   * @param prodNo the prod no
   * @return the product commission
   */
  QueryB2bB2ePriceResult getProductCommission(@Param("personId") String personId,
      @Param("prodNo") String prodNo);

  // 查詢 TBLB2E_COMMISSION 中的折扣值
  /**
   * Gets the tblb2e commission.
   * 
   * @param personId the person id
   * @param prodNo the prod no
   * @return the tblb2e commission
   */
  QueryB2bB2ePriceResult getTblb2eCommission(@Param("personId") String personId,
      @Param("prodNo") String prodNo);
}
