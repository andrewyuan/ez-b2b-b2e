/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.config
 * @FileName: AppProperties.java
 * @author: treylin
 * @date: 2014/9/15, 下午 04:27:20
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import tw.com.eztravel.ezlogger.Logger;
import tw.com.eztravel.ezlogger.LoggerFactory;

/**
 * <pre>
 * AppProperties, TODO: add Class Javadoc here.
 * </pre>
 * 
 * @author Trey Lin
 */
@Configuration
public class AppProperties {

  /**
   * Property placeholder configurer.
   * 
   * @return the property placeholder configurer
   */
  @Bean
  public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
    PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
    String location = System.getenv("B2BB2E_CONF");
 // Resource視設定檔數量而定，若只為1個則需要註解第二段載入部分
    Resource[] resource = new Resource[1];

    if (location == null) {
      location = "config.properties";
      resource[0] = new ClassPathResource(location);
      logger.info("Use config file: classpath:" + location);
//		暫時不需要
//      location = "payment.properties";
//      resource[1] = new ClassPathResource(location);
//      logger.info("Use config file: classpath:" + location);
    } else {
      resource[0] = new FileSystemResource(location + "/jboss/config.properties");
      logger.info("Use config file: " + location + "/jboss/config.properties");
//      resource[1] = new FileSystemResource(location + "/jboss/payment.properties");
//      logger.info("Use config file: " + location + "/jboss/payment.properties");
    }
    configurer.setLocations(resource);
    return configurer;
  }

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(AppProperties.class);
}
