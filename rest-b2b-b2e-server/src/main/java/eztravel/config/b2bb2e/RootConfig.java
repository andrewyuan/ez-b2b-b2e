/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.config.b2bb2e
 * @FileName: RootConfig.java
 * @author:   treylin
 * @date:     2014/9/11, 上午 09:49:05
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.config.b2bb2e;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import eztravel.config.AppProperties;
import eztravel.config.server.CommonRootConfig;
import eztravel.core.service.b2b.B2bPriceService;
import eztravel.core.service.b2b.impl.B2bPriceServiceImpl;
import eztravel.core.service.b2bb2e.B2bB2ePriceService;
import eztravel.core.service.b2bb2e.impl.B2bB2ePriceServiceImpl;
import eztravel.core.service.b2e.B2eGrantsService;
import eztravel.core.service.b2e.B2eInfoService;
import eztravel.core.service.b2e.B2ePriceService;
import eztravel.core.service.b2e.impl.B2eGrantsServiceImpl;
import eztravel.core.service.b2e.impl.B2eInfoServiceImpl;
import eztravel.core.service.b2e.impl.B2ePriceServiceImpl;
import eztravel.persistence.repository.b2b.B2bPriceRepository;
import eztravel.persistence.repository.b2e.B2ePriceRepository;



/**
 * <pre>
 * RootConfig,
 * </pre>
 * 
 * .
 * 
 * @author Trey Lin
 */
@Configuration
@Import({ AppProperties.class })
@EnableTransactionManagement
@MapperScan({"eztravel.persistence.repository.b2b", "eztravel.persistence.repository.b2e"})
public class RootConfig extends CommonRootConfig {

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(RootConfig.class);


  /**
   * B2b price service.
   * 
   * @param b2bPriceRepository the b2b price repository
   * @return the b2b price service
   */
  @Bean
  public B2bPriceService b2bPriceService(B2bPriceRepository b2bPriceRepository) {
    return new B2bPriceServiceImpl();
  }

  /**
   * B2e price service.
   * 
   * @param b2ePriceRepository the b2e price repository
   * @return the b2e price service
   */
  @Bean
  public B2ePriceService b2ePriceService(B2ePriceRepository b2ePriceRepository) {
    return new B2ePriceServiceImpl();
  }

  /**
   * B2b b2e price service.
   * 
   * @return the b2b b2e price service
   */
  @Bean
  public B2bB2ePriceService b2bB2ePriceService() {
    return new B2bB2ePriceServiceImpl();
  }
  
  /**
   * B2b b2e Grants service.
   * 
   * @return the B2b b2e Grants service
   */
  @Bean
  public B2eGrantsService b2eGrantsService() {
    return new B2eGrantsServiceImpl();
  }
  
  /**
   * B2e Info service.
   * 
   * @return the B2b b2e Grants service
   */
  @Bean
  public B2eInfoService b2eInfoService() {
    return new B2eInfoServiceImpl();
  }
}
