/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.b2b
 * @FileName: B2bPriceService.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2b;

import java.math.BigDecimal;

/**
 * The Interface B2bPriceService.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface B2bPriceService {

  // 取得 B2B 企業折扣值
  /**
   * Gets the b2b commission.
   * 
   * @param subType the sub type
   * @param prod_no the prod_no
   * @return the b2b commission
   */
  public BigDecimal getB2bCommission(String subType, String prod_no);

  // 查詢 B2B、B2E 狀態
  /**
   * Query b2b b2e yn.
   * 
   * @param prodNo the prod no
   * @return the string[]
   */
  public String[] queryB2bB2eYN(String prodNo);
  
  // 查詢 B2B、B2E 狀態（通用全線商品）(適合擁有"合格"的:1.上架日+下架日 2.上架日+null 3.null+下架日 4.上下架日皆沒有)
  /**
   * Query b2b b2e yn.
   * 
   * @param prodNo the prod no
   * @return the string[]
   */
  public String[] generalQueryB2bB2eYN(String prodNo);

}
