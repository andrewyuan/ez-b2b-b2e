/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.b2b.impl
 * @FileName: B2bPriceServiceImpl.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2b.impl;


import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.service.b2b.B2bPriceService;
import eztravel.persistence.repository.b2b.B2bPriceRepository;
import eztravel.rest.pojo.b2bb2e.QueryB2bB2ePriceResult;


/**
 * The Class B2bPriceServiceImpl.
 * 
 * <pre>
 * 
 * </pre>
 */
public class B2bPriceServiceImpl implements B2bPriceService {

  /** The b2b price repository. */
  @Autowired
  private B2bPriceRepository b2bPriceRepository;

  /** The Constant logger. */
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(B2bPriceServiceImpl.class);


  /*
   * B2B 同業佣金查詢規則:
   * 
   * 1.高鐵的流程(先判斷是否有設定該同業的佣金值) 2.其它非高鐵的商品的流程及高鐵流程中沒有設定佣金的廠商
   * (1).其它非高鐵流程的全商品(ex.FRN、FRT、GRP、GRT、ODT、HTL、HTF、FTK.......) (2).高鐵流程中，佣金表內「沒有」設定供應商品的佣金資訊
   * 3.如果是【國內旅遊、一日遊】的商品時，則查詢該商品的資訊...(處理國內旅遊離島線別的商品佣金寫死，不以佣金表為主) 4.票券商品及航空自由行流程中，不計算同業佣金 【極重要：
   * 更改此同業佣金規則後，務必 調整這支SP:USP_EC_ORD_B2B_PART_INSERT】
   * 
   * prod_no ：在高鐵商品會是供應商品編號 -> B2B000000XXXX 、其它流程則以原商品編號代入
   */
  // 取得 B2B 企業折扣值
  /* (non-Javadoc)
   * @see eztravel.core.service.b2b.B2bPriceService#getB2bCommission(java.lang.String, java.lang.String)
   */
  @Override
  public BigDecimal getB2bCommission(String subType, String prod_no) {

    BigDecimal b2bCommission;
    String commissionValue = "0"; // 同業佣金折扣值
    boolean checkHSRCompany = false; // 高鐵流程中，供應商是否有設定佣金值

    subType = (subType.trim().length() != 3) ? subType.substring(0, 3) : subType.trim(); // 商品副類別

    // 防呆，避免高鐵的流程不當代入ODT的副類別參數
    if (subType.equals("ODT")) {
      QueryB2bB2ePriceResult travelType = b2bPriceRepository.queryProdTravelType(prod_no);
      if (travelType != null) {
        subType = ("1 2 3 11".indexOf(travelType.getTravelType()) != -1) ? "ODT" : "HSR";
      }
    }


    // 1.高鐵的流程(先判斷是否有設定該供應商的佣金值)
    if (subType.equalsIgnoreCase("HSR") && !prod_no.equals("")) {
      QueryB2bB2ePriceResult hsrCompanyCommission =
          b2bPriceRepository.queryHSRCompanyCommission(prod_no);
      if (hsrCompanyCommission != null) {
        commissionValue = hsrCompanyCommission.getHsrCompanyCommission(); // 查詢高鐵同業設定的佣金值（供應商的佣金值(ex.10)）
        checkHSRCompany = true; // 檢核高鐵流程中，如果該供應商有設定佣金設定時，設為true...以此佣金為主（步驟2不執行）
      }
    }


    /*
     * 2.其它非高鐵的商品的流程及高鐵流程中沒有設定佣金的廠商 (1).其它非高鐵流程的全商品(ex.FRN、FRT、GRP、GRT、ODT、HTL、HTF、FTK.......)
     * (2).高鐵流程中，佣金表內「沒有」設定供應商品的佣金資訊
     */
    if (!checkHSRCompany) {

      List<QueryB2bB2ePriceResult> hmB2BCommis = b2bPriceRepository.queryB2BCommission(); // 查詢B2B同業佣金設定
      // 2.以該副類別的佣金設定為主(ex:FRN:6%)
      for (QueryB2bB2ePriceResult hmB2BCommi : hmB2BCommis) {
        if (subType.equals(hmB2BCommi.getCodeId())) {
          commissionValue = hmB2BCommi.getCommiValue();
        }
      }


      // 3.如果是【國內旅遊、一日遊】的商品時，則查詢該商品的資訊...(處理國內旅遊離島線別的商品佣金寫死，不以佣金表為主)
      if ("GRPGRTODT".indexOf(subType) != -1) {
        // 1.國內旅遊本島商品以原來的佣金表設定為主
        // 2.國內旅遊的花東蘭嶼綠島(線別:107)的商品(以區域景點做判斷)
        // (1).蘭嶼、綠島是 4%
        // (2).如果沒有設定或者是設定花蓮、台東時，是 8%
        // 3.澎湖金馬線別106是4%
        // 4.高鐵假期組為4%

        commissionValue =
            b2bPriceRepository.checkInternalGRPForInslandProd(prod_no) != null
                ? "4"
                : commissionValue; // 檢核國內旅遊商品是否為離島的商品
        commissionValue =
            b2bPriceRepository.checkInternalGRPForH05Prod(prod_no) != null ? "4" : commissionValue; // 檢核國內旅遊商品是否為高鐵假期組的商品
      }

      // 4.票券商品及航空自由行流程中，不計算同業佣金
      if ("TK".equalsIgnoreCase(subType.substring(0, 2)) || "AIT".equalsIgnoreCase(subType)) {
        return new BigDecimal("1.0");
      }

      QueryB2bB2ePriceResult businessProdYN = b2bPriceRepository.isB2BProd(prod_no);
      boolean isB2BProd = false;
      if (businessProdYN != null) {
        isB2BProd = "Y".equals(businessProdYN.getBusinessProdYN()) ? true : false; // ERP 沒有勾選同業銷售設定
      }
      if ("HTF".equals(subType) && !isB2BProd) {
        return new BigDecimal("1.0");
      }

    }
    b2bCommission =
        new BigDecimal(100 - Integer.parseInt(commissionValue)).divide(new BigDecimal("100"));

    return b2bCommission;
  }



  // 查詢 B2B、B2E 狀態
  /* (non-Javadoc)
   * @see eztravel.core.service.b2b.B2bPriceService#queryB2bB2eYN(java.lang.String)
   */
  @Override
  public String[] queryB2bB2eYN(String prodNo) {

    String[] b2bB2eYNResult = new String[] {"N", "N"};

    // 範例：GRT0000006617（正式台）
    QueryB2bB2ePriceResult b2bB2eYN = b2bPriceRepository.queryB2bB2eYN(prodNo);
    if (b2bB2eYN != null) {
      b2bB2eYNResult[0] = b2bB2eYN.getB2bYN();
      b2bB2eYNResult[1] = b2bB2eYN.getB2eYN();
    }

    return b2bB2eYNResult;
  }

  // 查詢 B2B、B2E 狀態（通用全線商品）(適合擁有"合格"的:1.上架日+下架日 2.上架日+null 3.null+下架日 4.上下架日皆沒有)
  /* (non-Javadoc)
   * @see eztravel.core.service.b2b.B2bPriceService#queryB2bB2eYN(java.lang.String)
   */
  @Override
  public String[] generalQueryB2bB2eYN(String prodNo) {

    String[] b2bB2eYNResult = new String[] {"N", "N"};

    // 範例：GRT0000006617（正式台）
    QueryB2bB2ePriceResult b2bB2eYN = b2bPriceRepository.generalQueryB2bB2eYN(prodNo);
    if (b2bB2eYN != null) {
      b2bB2eYNResult[0] = b2bB2eYN.getB2bYN();
      b2bB2eYNResult[1] = b2bB2eYN.getB2eYN();
    }

    return b2bB2eYNResult;
  }

  
}
