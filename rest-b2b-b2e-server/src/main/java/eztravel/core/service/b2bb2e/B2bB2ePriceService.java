/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.b2bb2e
 * @FileName: B2bB2ePriceService.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:17
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2bb2e;

import java.math.BigDecimal;

import eztravel.rest.enums.common.FareType;

/**
 * The Interface B2bB2ePriceService.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface B2bB2ePriceService {

  /**
   * Gets the b2b b2e price.
   * 
   * @param businessType the business type
   * @param price the price
   * @param prodNo the prod no
   * @param loginID the login id
   * @return the b2b b2e price
   */
  BigDecimal getB2bB2ePrice(FareType businessType, BigDecimal price, String prodNo, String loginID);

  /**
   * Gets the b2b b2e price for general prod.
   * 
   * @param businessType the business type
   * @param price the price
   * @param prodNo the prod no
   * @param loginID the login id
   * @return the b2b b2e price
   */
  BigDecimal getGeneralB2bB2ePrice(FareType businessType, BigDecimal price, String prodNo, String loginID);
}
