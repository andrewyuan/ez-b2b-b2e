/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.b2bb2e.impl
 * @FileName: B2bB2ePriceServiceImpl.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:13
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2bb2e.impl;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.service.b2b.B2bPriceService;
import eztravel.core.service.b2bb2e.B2bB2ePriceService;
import eztravel.core.service.b2e.B2ePriceService;
import eztravel.rest.enums.common.FareType;

/**
 * The Class B2bB2ePriceServiceImpl.
 * 
 * <pre>
 * 
 * </pre>
 */
public class B2bB2ePriceServiceImpl implements B2bB2ePriceService {

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(B2bB2ePriceServiceImpl.class);

  /** The b2b price service. */
  @Autowired
  private B2bPriceService b2bPriceService;

  /** The b2e price service. */
  @Autowired
  private B2ePriceService b2ePriceService;



  // 取得會員折扣價
  /* (non-Javadoc)
   * @see eztravel.core.service.b2bb2e.B2bB2ePriceService#getB2bB2ePrice(eztravel.rest.enums.common.FareType, java.math.BigDecimal, java.lang.String, java.lang.String)
   */
  @Override
  public BigDecimal getB2bB2ePrice(FareType businessType, BigDecimal price, String prodNo,
      String loginID) {

    // 判斷商品是否有設定會員折扣
    String[] b2bB2eYN = b2bPriceService.queryB2bB2eYN(prodNo);
    boolean b2bYN = "Y".equals(b2bB2eYN[0]) ? true : false;
    boolean b2eYN = "Y".equals(b2bB2eYN[1]) ? true : false;

    // B2B（先取得折扣值，再*原價）（範例：b2bPrice.getB2bCommission("GRT0000005799","GRT0000005799")）
    if (FareType.B2B.equals(businessType) && b2bYN) {

      price = b2bPriceService.getB2bCommission(prodNo, prodNo).multiply(price);
      logger.debug("B2BPrice：" + price);

      // B2E（先取得折扣值，再*原價）（範例：b2ePrice.getB2eCommission("TEST999", "GRT0000002987")）
    } else if (FareType.B2E.equals(businessType) && b2eYN) {

      price = b2ePriceService.getB2eCommission(loginID, prodNo).multiply(price);
      logger.debug("B2EPrice：" + price);

      // B2C
    } else {}

    return price;
  }
  
  // 取得會員折扣價（通用全線商品）(適合擁有"合格"的:1.上架日+下架日 2.上架日+null 3.null+下架日 4.上下架日皆沒有)
  /* (non-Javadoc)
   * @see eztravel.core.service.b2bb2e.B2bB2ePriceService#getGeneralProdB2bB2ePrice(eztravel.rest.enums.common.FareType, java.math.BigDecimal, java.lang.String, java.lang.String)
   */
  @Override
  public BigDecimal getGeneralB2bB2ePrice(FareType businessType, BigDecimal price, String prodNo,
      String loginID) {

    // 判斷商品是否有設定會員折扣
    String[] b2bB2eYN = b2bPriceService.generalQueryB2bB2eYN(prodNo);
    boolean b2bYN = "Y".equals(b2bB2eYN[0]) ? true : false;
    boolean b2eYN = "Y".equals(b2bB2eYN[1]) ? true : false;

    // B2B（先取得折扣值，再*原價）（範例：b2bPrice.getB2bCommission("GRT0000005799","GRT0000005799")）
    if (FareType.B2B.equals(businessType) && b2bYN) {

      price = b2bPriceService.getB2bCommission(prodNo, prodNo).multiply(price);
      logger.debug("B2BPrice：" + price);

      // B2E（先取得折扣值，再*原價）（範例：b2ePrice.getB2eCommission("TEST999", "GRT0000002987")）
    } else if (FareType.B2E.equals(businessType) && b2eYN) {

      price = b2ePriceService.getB2eCommission(loginID, prodNo).multiply(price);
      logger.debug("B2EPrice：" + price);

      // B2C
    } else {}

    return price;
  }
}
