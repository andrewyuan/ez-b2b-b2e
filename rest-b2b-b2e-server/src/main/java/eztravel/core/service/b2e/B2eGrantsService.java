/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.member
 * @FileName: B2eGrantsService.java
 * @author:   Kent
 * @date:     2014/2/11, 下午 02:17:57
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2e;

import java.math.BigDecimal;
import java.util.List;

import eztravel.rest.pojo.CodeDetail;
import eztravel.rest.pojo.b2e.B2eContact;
import eztravel.rest.pojo.b2e.B2eEmployee;
import eztravel.rest.pojo.b2e.B2eEmployeeGrant;
import eztravel.rest.pojo.b2e.GrantsSetting;


/**
 * 企業補助款.
 * 
 * @author Kent
 */
public interface B2eGrantsService {


  /**
   * 補助對象.
   * 
   * @return the list
   */
  public List<CodeDetail> listGrantsTarget();
  

  /**
   * 企業補助款相關設定資訊.
   * 
   * @param dealerNo the dealer no
   * @return the grants setting
   */
  public GrantsSetting getGrantsSetting(String dealerNo);

  /**
   * 企業部門.
   * 
   * @param dealerNo the dealer no
   * @return the list
   */
  public List<String> listDepartment(String dealerNo);

  /**
   * 企業員工.
   * 
   * @param dealerNo the dealer no
   * @param empName the emp name
   * @return the list
   */
  public List<B2eEmployee> listEmployee(String dealerNo, String empName);

  /**
   * 取得指定企業員工.
   * 
   * @param dealerNo the dealer no
   * @param empId the emp id
   * @return the employee
   */
  public B2eEmployee getEmployee(String dealerNo, String empId);

  /**
   * 取得指定企業員工當年度額度.
   * 
   * @param dealerNo the dealer no
   * @param empId the emp id
   * @return the employee grant
   */
  public B2eEmployeeGrant getEmployeeGrant(String dealerNo, String empId);

  /**
   * 取得指定企業員工當年度已使用額度.
   * 
   * @param dealerNo the dealer no
   * @param empId the emp id
   * @return the employee grant used
   */
  public BigDecimal getEmployeeGrantUsed(String dealerNo, String empId);


  /**
   * 取得企業福委窗口.
   * 
   * @param dealerNo the dealer no
   * @return the b2e contact
   */
  public B2eContact getB2eContact(String dealerNo);
}
