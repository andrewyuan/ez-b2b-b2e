/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.b2e.impl
 * @FileName: B2ePriceServiceImpl.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2e.impl;


import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.service.b2e.B2ePriceService;
import eztravel.persistence.repository.b2e.B2ePriceRepository;
import eztravel.rest.pojo.b2bb2e.QueryB2bB2ePriceResult;



/**
 * The Class B2ePriceServiceImpl.
 * 
 * <pre>
 * 
 * </pre>
 */
public class B2ePriceServiceImpl implements B2ePriceService {

  /** The b2e price repository. */
  @Autowired
  private B2ePriceRepository b2ePriceRepository;

  /** The logger. */
  Logger logger = LoggerFactory.getLogger(B2ePriceServiceImpl.class);


  // 取得 B2E 企業折扣值
  /* (non-Javadoc)
   * @see eztravel.core.service.b2e.B2ePriceService#getB2eCommission(java.lang.String, java.lang.String)
   */
  @Override
  public BigDecimal getB2eCommission(String loginID, String prodNo) {
    /*
     * 企業折扣規則：
     * 
     * 規則1：tblb2e_main.cust_status 須為'N',否則沒有企業折扣 A:申請中、E:中止、N:正常、R:異常、T:拒絕（tblcode_detail.item_id =
     * 'C27'） 排除「HTL」商品（HTL商品另有規則）
     * 
     * 規則2：查詢 TBLB2E_COMMISSION_PROD 中的折扣值
     * 
     * 規則3： 步驟(1) 查詢 TBLPRO 的商品線別(203)、副類別(FRT) 步驟(2) 查詢 TBLB2E_MAIN 的會員level 步驟(3) 查詢
     * TBLB2E_COMMISSION 中的折扣值 的折扣值(ex.2)（利用上述資訊）
     * 
     * ★ 結論： (1)如果規則1該會員不符合資格，不再執行規則2及規則3 的查詢。 (2)如果規則1該會員符合資格，則優先權為規則2 > 規則3。
     * 
     * ★ 結果：例如DB佣金折扣為 3, 則回傳(100-3)/100 = 0.97，再乘以商品原價
     */
    BigDecimal discountB2e = new BigDecimal("0.0");


    QueryB2bB2ePriceResult custStatus = b2ePriceRepository.isAvailableDealerNo(loginID);
    boolean isAvailableDealerNo = false;
    if (custStatus != null) {
      isAvailableDealerNo = "N".equals(custStatus.getCustStatus()) ? true : false;
    }
    logger.debug("isAvailableDealerNo：" + isAvailableDealerNo + "\nsubType："
        + prodNo.substring(0, 3));



    // 規則1：custNo為有效會員、非「HTL」商品（HTL商品另有規則）
    if (isAvailableDealerNo && !"HTL".equals(prodNo.substring(0, 3))) {

      // 規則2：從 TBLB2E_COMMISSION_PROD 中取得折扣值
      QueryB2bB2ePriceResult productCommission =
          b2ePriceRepository.getProductCommission(loginID, prodNo);
      if (productCommission != null) {
        discountB2e = productCommission.getProductCommission();
      } else {
        // 規則3：若 TBLB2E_COMMISSION_PROD 中取不到，才由 TBLB2E_COMMISSION 取 （範例："TEST999", "GRT0000002987"）
        QueryB2bB2ePriceResult tblb2eCommission =
            b2ePriceRepository.getTblb2eCommission(loginID, prodNo);
        if (tblb2eCommission != null) {
          discountB2e = tblb2eCommission.getTblb2eCommission();
        }
      }
    }

    discountB2e = (new BigDecimal("100").subtract(discountB2e)).divide(new BigDecimal("100")); // ex.DB中的值為2，則回傳
                                                                                               // (100-2)/100
    return discountB2e;
  }



}
