/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.b2e.impl
 * @FileName: B2eGrantsServiceImpl.java
 * @author:   Kent
 * @date:     2014/2/11, 下午 02:18:10
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2e.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import eztravel.core.service.b2e.B2eGrantsService;
import eztravel.persistence.repository.b2e.B2eGrantsRepository;
import eztravel.rest.pojo.CodeDetail;
import eztravel.rest.pojo.b2e.B2eContact;
import eztravel.rest.pojo.b2e.B2eEmployee;
import eztravel.rest.pojo.b2e.B2eEmployeeGrant;
import eztravel.rest.pojo.b2e.GrantsSetting;

/**
 * The Class B2eGrantsServiceImpl.
 * 
 * <pre>
 * 
 * </pre>
 * 
 * @author Kent
 */
public class B2eGrantsServiceImpl implements B2eGrantsService {

  /** The b2e grants repo. */
  @Autowired
  private B2eGrantsRepository b2eGrantsRepo;

  /**
   * Instantiates a new b2e grants service impl.
   */
  public B2eGrantsServiceImpl() {
    super();
  }

  /**
   * Instantiates a new b2e grants service impl.
   * 
   * @param b2eGrantsRepo the b2e grants repo
   */
  public B2eGrantsServiceImpl(B2eGrantsRepository b2eGrantsRepo) {
    super();
    this.b2eGrantsRepo = b2eGrantsRepo;
  }

  /*
   * (non-Javadoc)
   * 
   * @see eztravel.core.service.b2e.B2eGrantsService#listGrantsTarget()
   */
  @Override
  public List<CodeDetail> listGrantsTarget() {
    return b2eGrantsRepo.listGrantsTarget();
  }

  /*
   * (non-Javadoc)
   * 
   * @see eztravel.core.service.b2e.B2eGrantsService#getGrantsSetting(java.lang.String)
   */
  @Override
  public GrantsSetting getGrantsSetting(String dealerNo) {
    return b2eGrantsRepo.getGrantsSetting(dealerNo);
  }

  /*
   * (non-Javadoc)
   * 
   * @see eztravel.core.service.b2e.B2eGrantsService#listDepartment(java.lang.String)
   */
  @Override
  public List<String> listDepartment(String dealerNo) {
    return b2eGrantsRepo.listDepartment(dealerNo);
  }

  /*
   * (non-Javadoc)
   * 
   * @see eztravel.core.service.b2e.B2eGrantsService#listEmployee(java.lang.String,
   * java.lang.String)
   */
  @Override
  public List<B2eEmployee> listEmployee(String dealerNo, String empName) {
    if (!StringUtils.isEmpty(empName)) {
      return b2eGrantsRepo.listEmployeeByName(dealerNo, empName);
    } else {
      return b2eGrantsRepo.listEmployee(dealerNo);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see eztravel.core.service.b2e.B2eGrantsService#getEmployee(java.lang.String,
   * java.lang.String)
   */
  @Override
  public B2eEmployee getEmployee(String dealerNo, String empId) {
    return b2eGrantsRepo.getEmployee(dealerNo, empId);
  }

  /*
   * (non-Javadoc)
   * 
   * @see eztravel.core.service.b2e.B2eGrantsService#getEmployeeGrant(java.lang.String,
   * java.lang.String)
   */
  @Override
  public B2eEmployeeGrant getEmployeeGrant(String dealerNo, String empId) {
    return b2eGrantsRepo.getEmployeeGrant(dealerNo, empId);
  }

  /*
   * (non-Javadoc)
   * 
   * @see eztravel.core.service.b2e.B2eGrantsService#getEmployeeGrantUsed(java.lang.String,
   * java.lang.String)
   */
  @Override
  public BigDecimal getEmployeeGrantUsed(String dealerNo, String empId) {
    return b2eGrantsRepo.getEmployeeGrantUsed(dealerNo, empId);
  }

  /* (non-Javadoc)
   * @see eztravel.core.service.b2e.B2eGrantsService#getB2eContact(java.lang.String)
   */
  @Override
  public B2eContact getB2eContact(String dealerNo) {
    return b2eGrantsRepo.getB2eContact(dealerNo);
  }

}
