/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.b2e.impl
 * @FileName: B2eInfoServiceImpl.java
 * @author:   treylin
 * @date:     2014/9/16, 下午 12:47:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2e.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eztravel.core.service.b2e.B2eInfoService;
import eztravel.persistence.repository.b2e.B2eInfoRepository;
import eztravel.rest.pojo.b2e.B2EInfo;

/**
 * The Class B2eInfoServiceImpl.
 * 
 * <pre>
 * 
 * </pre>
 */
public class B2eInfoServiceImpl implements B2eInfoService {

	/** The b2e Info repository. */
	@Autowired
	private B2eInfoRepository b2eInfoRepository;

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(B2eInfoServiceImpl.class);

	  // 功能: 查詢B2E付款流程圖URL
	  /*
	   * (non-Javadoc)
	   * 
	   * @see eztravel.core.service.b2e.B2eInfoService#queryB2EPayFlowChartInfo(java.lang.String)
	   */
	@Override
	public List<B2EInfo> queryB2EPayFlowChartInfo(String personId) {
		List<B2EInfo> b2eDetail = b2eInfoRepository
				.queryB2EPayFlowChartInfo(personId);
		return b2eDetail;

	}

}
