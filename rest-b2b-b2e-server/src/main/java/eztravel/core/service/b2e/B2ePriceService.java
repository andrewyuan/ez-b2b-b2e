/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.service.b2e
 * @FileName: B2ePriceService.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:17
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.service.b2e;

import java.math.BigDecimal;

/**
 * The Interface B2ePriceService.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface B2ePriceService {

  // 取得 B2E 企業折扣值
  /**
   * 取得 B2E 企業折扣值. Gets the b2e commission.
   * 
   * @param loginID the login id
   * @param prodNo the prod no
   * @return the b2e commission
   */
  public BigDecimal getB2eCommission(String loginID, String prodNo);

}
