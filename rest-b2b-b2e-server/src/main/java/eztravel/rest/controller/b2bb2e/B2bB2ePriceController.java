/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.controller.member
 * @FileName: B2eGrantsController.java
 * @author: Kent
 * @date: 2014/1/15, 下午 03:47:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.controller.b2bb2e;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eztravel.core.service.b2bb2e.B2bB2ePriceService;
import eztravel.rest.controller.common.RestException;
import eztravel.rest.enums.common.FareType;
import eztravel.rest.pojo.b2bb2e.PriceInfo;
import eztravel.rest.pojo.common.RestResource;
import eztravel.rest.pojo.common.RestResourceFactory;



/**
 * 企業補助款.
 * 
 * <pre>

 * </pre>
 * 
 * @author Kent
 */
@Controller
@RequestMapping(value = "/price")
public class B2bB2ePriceController {

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(B2bB2ePriceController.class);

  /** The Constant STATUS_CODE. */
  private static final int STATUS_CODE = 200;
  
  /** The b2b b2e price service. */
  @Autowired
  private B2bB2ePriceService b2bB2ePriceService;
  
  // 功能: 取得會員折扣價
  /**
   * Get member discount price.
   * 
   * @param businessType the business type
   * @param price the price
   * @param prodNo the prod no
   * @param loginID the login id
   * @return the response entity
   */
  @RequestMapping(method = RequestMethod.GET, value = "/{businessType}/{price}/{prodNo}/{loginID}")
  public ResponseEntity<RestResource<PriceInfo>> queryMemberPrice(
      @PathVariable String businessType, @PathVariable BigDecimal price,
      @PathVariable String prodNo, @PathVariable String loginID) {

    // B2B（先取得折扣值，再*原價）（範例：b2bPrice.getB2bCommission("GRT0000005799","GRT0000005799")）
    if ("B2B".equals(businessType)) {
      price = b2bB2ePriceService.getB2bB2ePrice(FareType.B2B, price, prodNo, loginID);
    } else if ("B2E".equals(businessType)) {
      price = b2bB2ePriceService.getB2bB2ePrice(FareType.B2E, price, prodNo, loginID);
      // price = b2ePrice.getB2eCommission(loginID,
      // prodNo).multiply(price);
    } else {

    }

    RestResource<PriceInfo> body = RestResourceFactory.newInstance();

    try {
      List<PriceInfo> result = new ArrayList<>();
      PriceInfo pi = new PriceInfo();
      pi.setRole(businessType);
      pi.setPrice(price.toString());
      result.add(pi);
      body.setItems(result);
      body.setStatus(STATUS_CODE);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }

    return new ResponseEntity<RestResource<PriceInfo>>(body, HttpStatus.OK);
  }

  // 功能:  取得會員折扣價（通用全線商品）(適合擁有"合格"的:1.上架日+下架日 2.上架日+null 3.null+下架日 4.上下架日皆沒有)
  /**
   * Get member discount price.
   * 
   * @param businessType the business type
   * @param price the price
   * @param prodNo the prod no
   * @param loginID the login id
   * @return the response entity
   */
  @RequestMapping(method = RequestMethod.GET, value = "/general/{businessType}/{price}/{prodNo}/{loginID}")
  public ResponseEntity<RestResource<PriceInfo>> generalProdQueryMemberPrice(
      @PathVariable String businessType, @PathVariable BigDecimal price,
      @PathVariable String prodNo, @PathVariable String loginID) {

    // B2B（先取得折扣值，再*原價）（範例：b2bPrice.getB2bCommission("GRT0000005799","GRT0000005799")）
    if ("B2B".equals(businessType)) {
      price = b2bB2ePriceService.getGeneralB2bB2ePrice(FareType.B2B, price, prodNo, loginID);
    } else if ("B2E".equals(businessType)) {
      price = b2bB2ePriceService.getGeneralB2bB2ePrice(FareType.B2E, price, prodNo, loginID);
      // price = b2ePrice.getB2eCommission(loginID,
      // prodNo).multiply(price);
    } else {

    }

    RestResource<PriceInfo> body = RestResourceFactory.newInstance();

    try {
      List<PriceInfo> result = new ArrayList<>();
      PriceInfo pi = new PriceInfo();
      pi.setRole(businessType);
      pi.setPrice(price.toString());
      result.add(pi);
      body.setItems(result);
      body.setStatus(STATUS_CODE);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }

    return new ResponseEntity<RestResource<PriceInfo>>(body, HttpStatus.OK);
  }
}
