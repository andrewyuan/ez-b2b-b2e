/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.controller.member
 * @FileName: B2eGrantsController.java
 * @author: Kent
 * @date: 2014/1/15, 下午 03:47:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.controller.b2e;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eztravel.core.service.b2e.B2eGrantsService;
import eztravel.rest.controller.common.RestException;
import eztravel.rest.pojo.CodeDetail;
import eztravel.rest.pojo.b2e.B2eContact;
import eztravel.rest.pojo.b2e.B2eEmployee;
import eztravel.rest.pojo.b2e.B2eEmployeeGrant;
import eztravel.rest.pojo.b2e.GrantsB2eContactInfo;
import eztravel.rest.pojo.b2e.GrantsEmployeeGrantInfo;
import eztravel.rest.pojo.b2e.GrantsEmployeeInfo;
import eztravel.rest.pojo.b2e.GrantsInfo;
import eztravel.rest.pojo.b2e.GrantsSetting;
import eztravel.rest.pojo.b2e.GrantsTarget;
import eztravel.rest.pojo.common.RestResource;
import eztravel.rest.pojo.common.RestResourceFactory;


/**
 * 企業補助款.
 * 
 * <pre>
 * grants/target 企業補助款對象
 * grants/{dealerNo} 企業補助款相關設定
 * grants/{dealerNo}/department 部門
 * grants/{dealerNo}/employee?empName= 員工 
 * grants/{dealerNo}/employee/{empId} 員工
 * grants/{dealerNo}/employee/{empId}/grant 補助金額度、已使用額度
 * grants/{dealerNo}/b2eContact  取得企業的補助款福委窗口
 * </pre>
 * 
 * @author Kent
 */
@Controller
@RequestMapping(value = "/grants")
public class B2eGrantsController {

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(B2eGrantsController.class);

  /** The b2e grants service. */
  @Autowired
  private B2eGrantsService b2eGrantsService;

  /**
   * 企業助補款對象.
   * 
   * @param dealerNo the dealer no
   * @return the response entity
   */
  @RequestMapping(value = "/target", method = RequestMethod.GET)
  public ResponseEntity<RestResource<GrantsTarget>> listGrantsTarget() {
    RestResource<GrantsTarget> body = RestResourceFactory.newInstance();
    try {
      List<GrantsTarget> results = new ArrayList<GrantsTarget>();
      List<CodeDetail> codeDetails = b2eGrantsService.listGrantsTarget();
      for (CodeDetail cd : codeDetails) {
        GrantsTarget grantsTarget = new GrantsTarget(cd.getCode(), cd.getName());
        results.add(grantsTarget);
      }
      body.setItems(results);
      body.setStatus(200);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }
    return new ResponseEntity<RestResource<GrantsTarget>>(body, HttpStatus.OK);
  }

  /**
   * 企業補助款相關設定資訊.
   * 
   * @param dealerNo the dealer no
   * @return the grants setting
   */
  @RequestMapping(value = "/{dealerNo}", method = RequestMethod.GET)
  public ResponseEntity<RestResource<GrantsInfo>> getGrantsSetting(@PathVariable String dealerNo) {
    RestResource<GrantsInfo> body = RestResourceFactory.newInstance();
    try {

      List<GrantsInfo> results = new ArrayList<GrantsInfo>();
      GrantsSetting grantsSetting = b2eGrantsService.getGrantsSetting(dealerNo);
      GrantsInfo grantsInfo =
          new GrantsInfo(dealerNo, grantsSetting.getAgreeType(), grantsSetting.getSubsidySet(),
              grantsSetting.getCompanyNameChn(), grantsSetting.getCompanyNameEng(),
              grantsSetting.getSubsidyImg(), grantsSetting.getGrantsBegDate(),
              grantsSetting.getGrantsEndDate());

      results.add(grantsInfo);
      body.setItems(results);
      body.setStatus(200);
    } catch (Exception e) {
      e.printStackTrace();
      throw new RestException(e.getMessage(), body);
    }
    return new ResponseEntity<RestResource<GrantsInfo>>(body, HttpStatus.OK);
  }

  /**
   * 企業會員所擁有的部門.
   * 
   * @param dealerNo the dealer no
   * @return the response entity
   */
  @RequestMapping(value = "/{dealerNo}/department", method = RequestMethod.GET)
  public ResponseEntity<RestResource<String>> listGrantsDepartment(@PathVariable String dealerNo) {
    RestResource<String> body = RestResourceFactory.newInstance();
    try {
      List<String> results = b2eGrantsService.listDepartment(dealerNo);
      body.setItems(results);
      body.setStatus(200);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }
    return new ResponseEntity<RestResource<String>>(body, HttpStatus.OK);
  }

  /**
   * 企業會員所擁有的員工列表.(by name)
   * 
   * @param dealerNo the dealer no
   * @return the response entity
   */
  @RequestMapping(value = "/{dealerNo}/employee", method = RequestMethod.GET)
  public ResponseEntity<RestResource<GrantsEmployeeInfo>> listGrantsEmployee(
      @PathVariable String dealerNo, String name) {
    RestResource<GrantsEmployeeInfo> body = RestResourceFactory.newInstance();
    try {
      List<GrantsEmployeeInfo> results = new ArrayList<GrantsEmployeeInfo>();
      if(name == null) name="";
      List<B2eEmployee> b2eEmps = b2eGrantsService.listEmployee(dealerNo, java.net.URLDecoder.decode(name, "UTF-8"));
      for (B2eEmployee b2eEmp : b2eEmps) {
        GrantsEmployeeInfo gEmpInfo =
            new GrantsEmployeeInfo(b2eEmp.getDealerNo(), b2eEmp.getEmpId(), b2eEmp.getEmpName(),
                b2eEmp.getEmpDept(), b2eEmp.getFactoryArea(), b2eEmp.getMainLocation());
        results.add(gEmpInfo);
      }
      body.setItems(results);
      body.setStatus(200);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }
    return new ResponseEntity<RestResource<GrantsEmployeeInfo>>(body, HttpStatus.OK);
  }
  
  /**
   * 取得企業會員指定員工.
   * 
   * @param dealerNo the dealer no
   * @param empId the emp id
   * @return the b2e employee
   */
  @RequestMapping(value = "/{dealerNo}/employee/{empId}", method = RequestMethod.GET)
  public ResponseEntity<RestResource<GrantsEmployeeInfo>> getGrantsEmployee(
      @PathVariable String dealerNo, @PathVariable String empId) {
    RestResource<GrantsEmployeeInfo> body = RestResourceFactory.newInstance();
    try {
      List<GrantsEmployeeInfo> results = new ArrayList<GrantsEmployeeInfo>();
      B2eEmployee employee = b2eGrantsService.getEmployee(dealerNo, empId);
      if(employee != null){
        GrantsEmployeeInfo gEmpInfo =
            new GrantsEmployeeInfo(employee.getDealerNo(), employee.getEmpId(),
                employee.getEmpName(), employee.getEmpDept(), employee.getFactoryArea(),
                employee.getMainLocation());
        results.add(gEmpInfo);
      }
      body.setItems(results);
      body.setStatus(200);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }
    return new ResponseEntity<RestResource<GrantsEmployeeInfo>>(body, HttpStatus.OK);
  }
  
  /**
   * 取得員工補助金額度、已使用金額.
   * 
   * @param dealerNo the dealer no
   * @param empId the emp id
   * @return the employee grant
   */
  @RequestMapping(value = "/{dealerNo}/employee/{empId}/grant", method = RequestMethod.GET)
  public ResponseEntity<RestResource<GrantsEmployeeGrantInfo>> getEmployeeGrant(
      @PathVariable String dealerNo, @PathVariable String empId) {
    RestResource<GrantsEmployeeGrantInfo> body = RestResourceFactory.newInstance();
    try {
      List<GrantsEmployeeGrantInfo> results = new ArrayList<GrantsEmployeeGrantInfo>();
      B2eEmployeeGrant b2eEmpGrant = b2eGrantsService.getEmployeeGrant(dealerNo, empId);
      BigDecimal grantAmtUsed = b2eGrantsService.getEmployeeGrantUsed(dealerNo, empId);
      BigDecimal grantAmt = new BigDecimal(0);
      if (b2eEmpGrant != null) {
        grantAmt = b2eEmpGrant.getGrantAmt();
      }
      GrantsEmployeeGrantInfo gEmpGInfo = new GrantsEmployeeGrantInfo(grantAmt, grantAmtUsed);
      results.add(gEmpGInfo);
      body.setItems(results);
      body.setStatus(200);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }
    return new ResponseEntity<RestResource<GrantsEmployeeGrantInfo>>(body, HttpStatus.OK);
  }
  
  /**
   * 取得企業的補助款福委窗口.
   * 
   * @param dealerNo
   * @return
   */
  @RequestMapping(value = "/{dealerNo}/b2eContact", method = RequestMethod.GET)
  public ResponseEntity<RestResource<GrantsB2eContactInfo>> getB2eContact(@PathVariable String dealerNo) {
    RestResource<GrantsB2eContactInfo> body = RestResourceFactory.newInstance();
    try {
      List<GrantsB2eContactInfo> results = new ArrayList<GrantsB2eContactInfo>();
      B2eContact contact = b2eGrantsService.getB2eContact(dealerNo);
      GrantsB2eContactInfo contactInfo = new GrantsB2eContactInfo();
      contactInfo.setEmail(contact.getEmail());
      contactInfo.setName(contact.getName());
      results.add(contactInfo);
      body.setItems(results);
      body.setStatus(200);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }
    return new ResponseEntity<RestResource<GrantsB2eContactInfo>>(body, HttpStatus.OK);
  }

}
