/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.controller.member
 * @FileName: B2eInfoController.java
 * @author: treylin
 * @date: 2014/1/15, 下午 03:47:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.controller.b2e;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eztravel.core.service.b2e.B2eInfoService;
import eztravel.rest.controller.common.RestException;
import eztravel.rest.pojo.b2e.B2EInfo;
import eztravel.rest.pojo.common.RestResource;
import eztravel.rest.pojo.common.RestResourceFactory;



/**
 * 企業補助款.
 * 
 * <pre>

 * </pre>
 * 
 * @author Kent
 */
@Controller
@RequestMapping(value = "/b2einfo")
public class B2eInfoController {

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(B2eInfoController.class);

  /** The Constant STATUS_CODE. */
  private static final int STATUS_CODE = 200;
  
  /** The b2e info service. */
  @Autowired
  private B2eInfoService b2eInfoService;
  
  
  // 功能: 查詢B2E付款流程圖URL
  /**
   * Query b2 e pay flow chart info.
   * 
   * @param personId the person id
   * @return the response entity
   */
  @RequestMapping(method = RequestMethod.GET, value = "/{personId}")
  public ResponseEntity<RestResource<B2EInfo>> queryB2EPayFlowChartInfo(
      @PathVariable String personId) {

    RestResource<B2EInfo> body = RestResourceFactory.newInstance();

    try {
      body.setItems(this.b2eInfoService.queryB2EPayFlowChartInfo(personId));
      body.setStatus(STATUS_CODE);
    } catch (Exception e) {
      throw new RestException(e.getMessage(), body);
    }

    return new ResponseEntity<RestResource<B2EInfo>>(body, HttpStatus.OK);
  }
 
  
}
