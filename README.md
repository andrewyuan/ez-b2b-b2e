# Release Notes for ez-b2b-b2e

目錄：

[TOC]

## 各專案內容

* [ez-rest-b2b-b2e-api](ez-rest-b2b-b2e-api)
* [ez-rest-b2b-b2e-common](ez-rest-b2b-b2e-common)
* [rest-b2b-b2e-server](rest-b2b-b2e-server)

## **Version 1.1 (2014-09-18)**
	
> 完善API界面。
 	
#### Major Enhancemens

* API 加入 B2B B2E企業會員折扣計算 / B2E會員付款流程圖 #22f30d8
* 修正RESTful 伺服器 pom載入common版本號 #4f74446


## **Version 1.0 (2014-09-17)**
	
> **初始版本**，為了區分共用元件並管理各專案依存性，於是將有關B2B B2E邏輯從現有專案拆出。
> 
> 搬遷 [ez-a0001] B2BB2E相關程式及[ez-a0002] 租車專案之**補助款**業務邏輯至本專案，程式命名未更動。
> 
> 以下為搬遷列表：

**以下搬遷至ez-rest-b2b-b2e-api**

* from [ez-a0002]/rentcar 
	- src/main/java/app/service/`B2eGrantsService.java`
	- src/main/java/app/service/impl/`B2eGrantsServiceImpl.java`

**以下搬遷至ez-rest-b2b-b2e-common**

* from [ez-a0002]/ez-rest-rentcar-common
	- src/main/java/eztravel/rest/pojo/rentcar/

			B2EInfo.java
			PriceInfo.java
		
* from [ez-a0001]/ez-rest-member-common
	- src/main/java/eztravel/rest/pojo/member/b2e/
		
			GrantsEmployeeGrantInfo.java
			GrantsEmployeeInfo.java
			GrantsInfo.java
			GrantsTarget.java
			GrantsB2eContractInfo.java


* from [ez-a0001]/ez-rest-member-core/
	- src/main/java/eztravel/persistence/pojo/member/`CodeDetail.java`
	- src/main/java/eztravel/persistence/pojo/member/b2e
	
			B2eContact.java
			B2eEmployee.java
			B2eEmployeeGrant.java
			GrantsSetting.java
		 

**以下搬遷至rest-b2b-b2e-server**

* from [ez-a0001]/ez-rest-member-core
	- src/main/java/eztravel/core/service/member/`B2eGrantsService.java`
	- src/main/java/eztravel/core/service/member/impl/`B2eGrantsServiceImpl.java`
	- src/main/java/eztravel/persistence/repository/member/`B2eGrantsRepository.java`
	- src/main/resources/eztravel/persistence/repository/member/`B2eGrantsRepository.xml`
* from [ez-a0001]/rest-member-server
	- src/main/java/eztravel/rest/controller/member/`B2eGrantsController.java`
* from [ez-a0002]/rest-rentcar-server 
	- src/main/java/eztravel/core/service/rentcar/`RentCarService.java `
	- src/main/java/eztravel/core/service/rentcar/`RentCarServiceImpl.java `
	- src/main/java/eztravel/rest/controller/rentcar/`RentCarController.java `
	
	
	上述三個檔案分別對應`B2eInfoService`, `B2eInfoServiceImp`,`B2eInfoController`,方法名稱如下： `queryMemberPrice(),	queryB2EPayFlowChartInfo()`
	
	
[ez-a0001]: https://bitbucket.org/ezecteam/ez-a0001
[ez-a0002]: https://bitbucket.org/ezecteam/ez-a0002