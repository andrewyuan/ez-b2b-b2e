/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.pojo.member.b2e
 * @FileName: GrantsEmployeeInfo.java
 * @author:   Kent
 * @date:     2014/1/15, 下午 03:27:35
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2e;

/**
 * <pre> GrantsEmployeeInfo, TODO: add Class Javadoc here. </pre>
 *
 * @author Kent
 */
public class GrantsEmployeeInfo {

  /** The dealer no. */
  private String dealerNo; // 企業編號
  
  /** The id. */
  private String id; // 員工編號
  
  /** The name. */
  private String name; // 員工姓名
  
  /** The dept id. */
  private String deptId; // 部門
  
  /** The factory area. */
  private String factoryArea; // 廠區
  
  /** The location. */
  private String location; // main location

  /**
   * Instantiates a new grants employee info.
   */
  public GrantsEmployeeInfo() {
    super();
  }

  /**
   * Instantiates a new grants employee info.
   * 
   * @param dealerNo the dealer no
   * @param id the id
   * @param name the name
   * @param deptId the dept id
   * @param factoryArea the factory area
   * @param location the location
   */
  public GrantsEmployeeInfo(String dealerNo, String id, String name, String deptId,
      String factoryArea, String location) {
    super();
    this.dealerNo = dealerNo;
    this.id = id;
    this.name = name;
    this.deptId = deptId;
    this.factoryArea = factoryArea;
    this.location = location;
  }

  /**
   * Gets the dealer no.
   * 
   * @return the dealerNo
   */
  public String getDealerNo() {
    return dealerNo;
  }

  /**
   * Sets the dealer no.
   * 
   * @param dealerNo the dealerNo to set
   */
  public void setDealerNo(String dealerNo) {
    this.dealerNo = dealerNo;
  }

  /**
   * Gets the id.
   * 
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   * 
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the name.
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   * 
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Gets the dept id.
   * 
   * @return the deptId
   */
  public String getDeptId() {
    return deptId;
  }

  /**
   * Sets the dept id.
   * 
   * @param deptId the deptId to set
   */
  public void setDeptId(String deptId) {
    this.deptId = deptId;
  }

  /**
   * Gets the factory area.
   * 
   * @return the factoryArea
   */
  public String getFactoryArea() {
    return factoryArea;
  }

  /**
   * Sets the factory area.
   * 
   * @param factoryArea the factoryArea to set
   */
  public void setFactoryArea(String factoryArea) {
    this.factoryArea = factoryArea;
  }

  /**
   * Gets the location.
   * 
   * @return the location
   */
  public String getLocation() {
    return location;
  }

  /**
   * Sets the location.
   * 
   * @param location the location to set
   */
  public void setLocation(String location) {
    this.location = location;
  }
  
  
}
