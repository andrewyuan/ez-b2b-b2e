/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.pojo.member.b2e
 * @FileName: GrantsEmployeeGrantInfo.java
 * @author: Kent
 * @date: 2014/1/15, 下午 03:39:05
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2e;

import java.math.BigDecimal;

/**
 * <pre> GrantsEmployeeGrantInfo, 員工補助款額度、已使用金額. </pre>
 * 
 * @author Kent
 */
public class GrantsEmployeeGrantInfo {

  /** The grant amt. */
  private BigDecimal grantAmt; // 額度

  /** The grant amt used. */
  private BigDecimal grantAmtUsed; // 已使用額度

  /**
   * Instantiates a new grants employee grant info.
   */
  public GrantsEmployeeGrantInfo() {
    super();
  }

  /**
   * Instantiates a new grants employee grant info.
   * 
   * @param grantAmt the grant amt
   * @param grantAmtUsed the grant amt used
   */
  public GrantsEmployeeGrantInfo(BigDecimal grantAmt, BigDecimal grantAmtUsed) {
    super();
    this.grantAmt = grantAmt;
    this.grantAmtUsed = grantAmtUsed;
  }

  /**
   * Gets the grant amt.
   * 
   * @return the grantAmt
   */
  public BigDecimal getGrantAmt() {
    return grantAmt;
  }

  /**
   * Sets the grant amt.
   * 
   * @param grantAmt the grantAmt to set
   */
  public void setGrantAmt(BigDecimal grantAmt) {
    this.grantAmt = grantAmt;
  }

  /**
   * Gets the grant amt used.
   * 
   * @return the grantAmtUsed
   */
  public BigDecimal getGrantAmtUsed() {
    return grantAmtUsed;
  }

  /**
   * Sets the grant amt used.
   * 
   * @param grantAmtUsed the grantAmtUsed to set
   */
  public void setGrantAmtUsed(BigDecimal grantAmtUsed) {
    this.grantAmtUsed = grantAmtUsed;
  }

}
