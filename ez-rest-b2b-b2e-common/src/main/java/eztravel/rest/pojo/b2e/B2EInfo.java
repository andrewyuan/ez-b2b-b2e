/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: models.pojo
 * @FileName: B2EInfo.java
 * @author: tonywang
 * @date: 2013/12/15, 下午 03:24:05
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2e;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * B2E會員 旅客填寫訂購單,流程圖(URL).
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class B2EInfo implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5256804374970853899L;

  /** The dealer no. */
  private String dealerNo;

  /** The subsidy msg. */
  private String subsidyMsg;

  /**
   * Gets the dealer no.
   * 
   * @return the dealer no
   */
  public String getDealerNo() {
    return dealerNo;
  }

  /**
   * Sets the dealer no.
   * 
   * @param dealerNo the new dealer no
   */
  public void setDealerNo(String dealerNo) {
    this.dealerNo = dealerNo;
  }

  /**
   * Gets the subsidy msg.
   * 
   * @return the subsidy msg
   */
  public String getSubsidyMsg() {
    return subsidyMsg;
  }

  /**
   * Sets the subsidy msg.
   * 
   * @param subsidyMsg the new subsidy msg
   */
  public void setSubsidyMsg(String subsidyMsg) {
    this.subsidyMsg = subsidyMsg;
  }

}
