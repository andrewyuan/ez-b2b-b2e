/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.pojo.member.b2e
 * @FileName: GrantsB2eContactInfo.java
 * @author:   Claire
 * @date:     2014/8/19, 下午 03:23:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2e;

/**
 * <pre> B2eContact, TODO: add Class Javadoc here. </pre>
 *
 * @author Kent
 */
public class GrantsB2eContactInfo {

  /** The name. */
  private String name;
  
  /** The email. */
  private String email;
  
  /**
   * Gets the name.
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }
  
  /**
   * Sets the name.
   * 
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * Gets the email.
   * 
   * @return the email
   */
  public String getEmail() {
    return email;
  }
  
  /**
   * Sets the email.
   * 
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }
  
  
}
