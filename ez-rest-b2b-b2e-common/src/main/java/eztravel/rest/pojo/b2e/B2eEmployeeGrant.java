/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.persistence.pojo.member.b2e
 * @FileName: B2eEmployeeGrant.java
 * @author: Kent
 * @date: 2014/1/15, 下午 03:46:12
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2e;

import java.math.BigDecimal;

/**
 * <pre> B2eEmployeeGrant, TODO: add Class Javadoc here. </pre>
 * 
 * @author Kent
 */
public class B2eEmployeeGrant {

  /** The dealer no. */
  private String dealerNo;

  /** The grant year. */
  private String grantYear;

  /** The emp id. */
  private String empId;

  /** The grant amt. */
  private BigDecimal grantAmt;

  /** The wc amt. */
  private BigDecimal wcAmt;

  /** The comp amt. */
  private BigDecimal compAmt;

  /** The used amt. */
  private BigDecimal usedAmt;

  /** The remain amt. */
  private BigDecimal remainAmt;

  /** The remark. */
  private String remark;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private String createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private String modDt;

  /**
   * Gets the dealer no.
   * 
   * @return the dealerNo
   */
  public String getDealerNo() {
    return dealerNo;
  }

  /**
   * Sets the dealer no.
   * 
   * @param dealerNo the dealerNo to set
   */
  public void setDealerNo(String dealerNo) {
    this.dealerNo = dealerNo;
  }

  /**
   * Gets the grant year.
   * 
   * @return the grantYear
   */
  public String getGrantYear() {
    return grantYear;
  }

  /**
   * Sets the grant year.
   * 
   * @param grantYear the grantYear to set
   */
  public void setGrantYear(String grantYear) {
    this.grantYear = grantYear;
  }

  /**
   * Gets the emp id.
   * 
   * @return the empId
   */
  public String getEmpId() {
    return empId;
  }

  /**
   * Sets the emp id.
   * 
   * @param empId the empId to set
   */
  public void setEmpId(String empId) {
    this.empId = empId;
  }

  /**
   * Gets the grant amt.
   * 
   * @return the grantAmt
   */
  public BigDecimal getGrantAmt() {
    return grantAmt;
  }

  /**
   * Sets the grant amt.
   * 
   * @param grantAmt the grantAmt to set
   */
  public void setGrantAmt(BigDecimal grantAmt) {
    this.grantAmt = grantAmt;
  }

  /**
   * Gets the wc amt.
   * 
   * @return the wcAmt
   */
  public BigDecimal getWcAmt() {
    return wcAmt;
  }

  /**
   * Sets the wc amt.
   * 
   * @param wcAmt the wcAmt to set
   */
  public void setWcAmt(BigDecimal wcAmt) {
    this.wcAmt = wcAmt;
  }

  /**
   * Gets the comp amt.
   * 
   * @return the compAmt
   */
  public BigDecimal getCompAmt() {
    return compAmt;
  }

  /**
   * Sets the comp amt.
   * 
   * @param compAmt the compAmt to set
   */
  public void setCompAmt(BigDecimal compAmt) {
    this.compAmt = compAmt;
  }

  /**
   * Gets the used amt.
   * 
   * @return the usedAmt
   */
  public BigDecimal getUsedAmt() {
    return usedAmt;
  }

  /**
   * Sets the used amt.
   * 
   * @param usedAmt the usedAmt to set
   */
  public void setUsedAmt(BigDecimal usedAmt) {
    this.usedAmt = usedAmt;
  }

  /**
   * Gets the remain amt.
   * 
   * @return the remainAmt
   */
  public BigDecimal getRemainAmt() {
    return remainAmt;
  }

  /**
   * Sets the remain amt.
   * 
   * @param remainAmt the remainAmt to set
   */
  public void setRemainAmt(BigDecimal remainAmt) {
    this.remainAmt = remainAmt;
  }

  /**
   * Gets the remark.
   * 
   * @return the remark
   */
  public String getRemark() {
    return remark;
  }

  /**
   * Sets the remark.
   * 
   * @param remark the remark to set
   */
  public void setRemark(String remark) {
    this.remark = remark;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the creator to set
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the createDt
   */
  public String getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the createDt to set
   */
  public void setCreateDt(String createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the moder to set
   */
  public void setModer(String moder) {
    this.moder = moder;
  }

  /**
   * Gets the mod dt.
   * 
   * @return the modDt
   */
  public String getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the modDt to set
   */
  public void setModDt(String modDt) {
    this.modDt = modDt;
  }


}
