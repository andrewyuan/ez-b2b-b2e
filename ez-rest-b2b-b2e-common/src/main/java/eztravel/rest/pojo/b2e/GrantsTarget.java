/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.pojo.member.b2e
 * @FileName: GrantsTarget.java
 * @author: Kent
 * @date: 2014/1/15, 下午 03:06:23
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2e;

/**
 * <pre> GrantsTarget, 補助對象. </pre>
 * 
 * @author Kent
 */
public class GrantsTarget {

  /** The code. */
  private String code;

  /** The name. */
  private String name;

  /**
   * Instantiates a new grants target.
   */
  public GrantsTarget() {
    super();
  }

  /**
   * Instantiates a new grants target.
   * 
   * @param code the code
   * @param name the name
   */
  public GrantsTarget(String code, String name) {
    super();
    this.code = code;
    this.name = name;
  }

  /**
   * Gets the code.
   * 
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * Sets the code.
   * 
   * @param code the code to set
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * Gets the name.
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   * 
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }
}
