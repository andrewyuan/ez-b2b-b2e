/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.pojo.member.b2e
 * @FileName: GrantsInfo.java
 * @author: Kent
 * @date: 2014/1/15, 下午 03:13:39
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2e;

/**
 * <pre> GrantsInfo, 企業補助款相關設定資訊. </pre>
 * 
 * @author Kent
 */
public class GrantsInfo {

  /** 企業編號 */
  private String dealerNo;

  /** 合約類型 */
  private String agreeType;

  /** 補助款相關設定(NNNNN) */
  private String subsidySet;

  /** 公司中文名 */
  private String companyNameChn;

  /** 公司英文名 */
  private String companyNameEng;

  /** 補助款提示說明(image url) */
  private String subsidyImg;

  /** 補助使用期間起 */
  private String grantsBegDate;

  /** 補助使用期間迄 */
  private String grantsEndDate;

  /**
   * Instantiates a new grants info.
   */
  public GrantsInfo() {
    super();
  }

  /**
   * Instantiates a new grants info.
   * 
   * @param dealerNo the dealer no
   * @param agreeType the agree type
   * @param subsidySet the subsidy set
   * @param companyNameChn the company name chn
   * @param companyNameEng the company name eng
   * @param subsidyImg the subsidy img
   * @param grantsBegDate the grants beg date
   * @param grantsEndDate the grants end date
   */
  public GrantsInfo(String dealerNo, String agreeType, String subsidySet, String companyNameChn,
      String companyNameEng, String subsidyImg, String grantsBegDate, String grantsEndDate) {
    super();
    this.dealerNo = dealerNo;
    this.agreeType = agreeType;
    this.subsidySet = subsidySet;
    this.companyNameChn = companyNameChn;
    this.companyNameEng = companyNameEng;
    this.subsidyImg = subsidyImg;
    this.grantsBegDate = grantsBegDate;
    this.grantsEndDate = grantsEndDate;
  }

  /**
   * Gets the dealer no.
   * 
   * @return the dealerNo
   */
  public String getDealerNo() {
    return dealerNo;
  }

  /**
   * Sets the dealer no.
   * 
   * @param dealerNo the dealerNo to set
   */
  public void setDealerNo(String dealerNo) {
    this.dealerNo = dealerNo;
  }

  /**
   * Gets the agree type.
   * 
   * @return the agreeType
   */
  public String getAgreeType() {
    return agreeType;
  }

  /**
   * Sets the agree type.
   * 
   * @param agreeType the agreeType to set
   */
  public void setAgreeType(String agreeType) {
    this.agreeType = agreeType;
  }

  /**
   * Gets the subsidy set.
   * 
   * @return the subsidySet
   */
  public String getSubsidySet() {
    return subsidySet;
  }

  /**
   * Sets the subsidy set.
   * 
   * @param subsidySet the subsidySet to set
   */
  public void setSubsidySet(String subsidySet) {
    this.subsidySet = subsidySet;
  }

  /**
   * Gets the company name chn.
   * 
   * @return the companyNameChn
   */
  public String getCompanyNameChn() {
    return companyNameChn;
  }

  /**
   * Sets the company name chn.
   * 
   * @param companyNameChn the companyNameChn to set
   */
  public void setCompanyNameChn(String companyNameChn) {
    this.companyNameChn = companyNameChn;
  }

  /**
   * Gets the company name eng.
   * 
   * @return the companyNameEng
   */
  public String getCompanyNameEng() {
    return companyNameEng;
  }

  /**
   * Sets the company name eng.
   * 
   * @param companyNameEng the companyNameEng to set
   */
  public void setCompanyNameEng(String companyNameEng) {
    this.companyNameEng = companyNameEng;
  }

  /**
   * Gets the subsidy img.
   * 
   * @return the subsidyImg
   */
  public String getSubsidyImg() {
    return subsidyImg;
  }

  /**
   * Sets the subsidy img.
   * 
   * @param subsidyImg the subsidyImg to set
   */
  public void setSubsidyImg(String subsidyImg) {
    this.subsidyImg = subsidyImg;
  }

  /**
   * Gets the grants beg date.
   * 
   * @return the grantsBegDate
   */
  public String getGrantsBegDate() {
    return grantsBegDate;
  }

  /**
   * Sets the grants beg date.
   * 
   * @param grantsBegDate the grantsBegDate to set
   */
  public void setGrantsBegDate(String grantsBegDate) {
    this.grantsBegDate = grantsBegDate;
  }

  /**
   * Gets the grants end date.
   * 
   * @return the grantsEndDate
   */
  public String getGrantsEndDate() {
    return grantsEndDate;
  }

  /**
   * Sets the grants end date.
   * 
   * @param grantsEndDate the grantsEndDate to set
   */
  public void setGrantsEndDate(String grantsEndDate) {
    this.grantsEndDate = grantsEndDate;
  }

}
