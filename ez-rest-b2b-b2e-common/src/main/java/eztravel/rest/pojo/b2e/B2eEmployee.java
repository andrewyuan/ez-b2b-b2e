/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.pojo.member.b2e
 * @FileName: B2eEmployee.java
 * @author:   Kent
 * @date:     2014/1/15, 下午 12:13:51
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2e;

/**
 * <pre> B2eEmployee, TODO: add Class Javadoc here. </pre>
 *
 * @author Kent
 */
public class B2eEmployee {

  /** The dealer no. */
  private String dealerNo;
  
  /** The emp id. */
  private String empId;
  
  /** The emp name. */
  private String empName;
  
  /** The seq no. */
  private String seqNo;
  
  /** The emp dept. */
  private String empDept;
  
  /** The factory area. */
  private String factoryArea;
  
  /** The main location. */
  private String mainLocation;
  
  /** The remark. */
  private String remark;
  
  /** The creator. */
  private String creator;
  
  /** The create dt. */
  private String createDt;
  
  /** The moder. */
  private String moder;
  
  /** The mod dt. */
  private String modDt;
  
  /**
   * Gets the dealer no.
   * 
   * @return the dealerNo
   */
  public String getDealerNo() {
    return dealerNo;
  }
  
  /**
   * Sets the dealer no.
   * 
   * @param dealerNo the dealerNo to set
   */
  public void setDealerNo(String dealerNo) {
    this.dealerNo = dealerNo;
  }
  
  /**
   * Gets the emp id.
   * 
   * @return the empId
   */
  public String getEmpId() {
    return empId;
  }
  
  /**
   * Sets the emp id.
   * 
   * @param empId the empId to set
   */
  public void setEmpId(String empId) {
    this.empId = empId;
  }
  
  /**
   * Gets the emp name.
   * 
   * @return the empName
   */
  public String getEmpName() {
    return empName;
  }
  
  /**
   * Sets the emp name.
   * 
   * @param empName the empName to set
   */
  public void setEmpName(String empName) {
    this.empName = empName;
  }
  
  /**
   * Gets the seq no.
   * 
   * @return the seqNo
   */
  public String getSeqNo() {
    return seqNo;
  }
  
  /**
   * Sets the seq no.
   * 
   * @param seqNo the seqNo to set
   */
  public void setSeqNo(String seqNo) {
    this.seqNo = seqNo;
  }
  
  /**
   * Gets the emp dept.
   * 
   * @return the empDept
   */
  public String getEmpDept() {
    return empDept;
  }
  
  /**
   * Sets the emp dept.
   * 
   * @param empDept the empDept to set
   */
  public void setEmpDept(String empDept) {
    this.empDept = empDept;
  }
  
  /**
   * Gets the factory area.
   * 
   * @return the factoryArea
   */
  public String getFactoryArea() {
    return factoryArea;
  }
  
  /**
   * Sets the factory area.
   * 
   * @param factoryArea the factoryArea to set
   */
  public void setFactoryArea(String factoryArea) {
    this.factoryArea = factoryArea;
  }
  
  /**
   * Gets the main location.
   * 
   * @return the mainLocation
   */
  public String getMainLocation() {
    return mainLocation;
  }
  
  /**
   * Sets the main location.
   * 
   * @param mainLocation the mainLocation to set
   */
  public void setMainLocation(String mainLocation) {
    this.mainLocation = mainLocation;
  }
  
  /**
   * Gets the remark.
   * 
   * @return the remark
   */
  public String getRemark() {
    return remark;
  }
  
  /**
   * Sets the remark.
   * 
   * @param remark the remark to set
   */
  public void setRemark(String remark) {
    this.remark = remark;
  }
  
  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }
  
  /**
   * Sets the creator.
   * 
   * @param creator the creator to set
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }
  
  /**
   * Gets the creates the dt.
   * 
   * @return the createDt
   */
  public String getCreateDt() {
    return createDt;
  }
  
  /**
   * Sets the creates the dt.
   * 
   * @param createDt the createDt to set
   */
  public void setCreateDt(String createDt) {
    this.createDt = createDt;
  }
  
  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }
  
  /**
   * Sets the moder.
   * 
   * @param moder the moder to set
   */
  public void setModer(String moder) {
    this.moder = moder;
  }
  
  /**
   * Gets the mod dt.
   * 
   * @return the modDt
   */
  public String getModDt() {
    return modDt;
  }
  
  /**
   * Sets the mod dt.
   * 
   * @param modDt the modDt to set
   */
  public void setModDt(String modDt) {
    this.modDt = modDt;
  }
  
}
