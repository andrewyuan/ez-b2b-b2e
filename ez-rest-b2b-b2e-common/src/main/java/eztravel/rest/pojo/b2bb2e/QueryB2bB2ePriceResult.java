/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.b2bb2e
 * @FileName: QueryB2bB2ePriceResult.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:55:44
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2bb2e;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class QueryB2bB2ePriceResult.
 * 
 * <pre>
 * 
 * </pre>
 */
public class QueryB2bB2ePriceResult implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7306899158568310141L;

  /** The b2b yn. */
  String b2bYN;

  /** The b2e yn. */
  String b2eYN;

  /** The travel type. */
  String travelType;

  /** The hsr company commission. */
  String hsrCompanyCommission;

  /** The code id. */
  String codeId;

  /** The commi value. */
  String commiValue;

  /** The prod no. */
  String prodNo;

  /** The business prod yn. */
  String businessProdYN;

  /** The cust status. */
  String custStatus;

  /** The product commission. */
  BigDecimal productCommission;

  /** The tblb2e commission. */
  BigDecimal tblb2eCommission;

  /**
   * Gets the tblb2e commission.
   * 
   * @return the tblb2e commission
   */
  public BigDecimal getTblb2eCommission() {
    return tblb2eCommission;
  }

  /**
   * Sets the tblb2e commission.
   * 
   * @param tblb2eCommission the new tblb2e commission
   */
  public void setTblb2eCommission(BigDecimal tblb2eCommission) {
    this.tblb2eCommission = tblb2eCommission;
  }

  /**
   * Gets the product commission.
   * 
   * @return the product commission
   */
  public BigDecimal getProductCommission() {
    return productCommission;
  }

  /**
   * Sets the product commission.
   * 
   * @param productCommission the new product commission
   */
  public void setProductCommission(BigDecimal productCommission) {
    this.productCommission = productCommission;
  }

  /**
   * Gets the cust status.
   * 
   * @return the cust status
   */
  public String getCustStatus() {
    return custStatus;
  }

  /**
   * Sets the cust status.
   * 
   * @param custStatus the new cust status
   */
  public void setCustStatus(String custStatus) {
    this.custStatus = custStatus;
  }

  /**
   * Gets the business prod yn.
   * 
   * @return the business prod yn
   */
  public String getBusinessProdYN() {
    return businessProdYN;
  }

  /**
   * Sets the business prod yn.
   * 
   * @param businessProdYN the new business prod yn
   */
  public void setBusinessProdYN(String businessProdYN) {
    this.businessProdYN = businessProdYN;
  }

  /**
   * Gets the prod no.
   * 
   * @return the prod no
   */
  public String getProdNo() {
    return prodNo;
  }

  /**
   * Sets the prod no.
   * 
   * @param prodNo the new prod no
   */
  public void setProdNo(String prodNo) {
    this.prodNo = prodNo;
  }

  /**
   * Gets the code id.
   * 
   * @return the code id
   */
  public String getCodeId() {
    return codeId;
  }

  /**
   * Sets the code id.
   * 
   * @param codeId the new code id
   */
  public void setCodeId(String codeId) {
    this.codeId = codeId;
  }

  /**
   * Gets the commi value.
   * 
   * @return the commi value
   */
  public String getCommiValue() {
    return commiValue;
  }

  /**
   * Sets the commi value.
   * 
   * @param commiValue the new commi value
   */
  public void setCommiValue(String commiValue) {
    this.commiValue = commiValue;
  }

  /**
   * Gets the hsr company commission.
   * 
   * @return the hsr company commission
   */
  public String getHsrCompanyCommission() {
    return hsrCompanyCommission;
  }

  /**
   * Sets the hsr company commission.
   * 
   * @param hsrCompanyCommission the new hsr company commission
   */
  public void setHsrCompanyCommission(String hsrCompanyCommission) {
    this.hsrCompanyCommission = hsrCompanyCommission;
  }

  /**
   * Gets the travel type.
   * 
   * @return the travel type
   */
  public String getTravelType() {
    return travelType;
  }

  /**
   * Sets the travel type.
   * 
   * @param travelType the new travel type
   */
  public void setTravelType(String travelType) {
    this.travelType = travelType;
  }

  /**
   * Gets the b2b yn.
   * 
   * @return the b2b yn
   */
  public String getB2bYN() {
    return b2bYN;
  }

  /**
   * Sets the b2b yn.
   * 
   * @param b2bYN the new b2b yn
   */
  public void setB2bYN(String b2bYN) {
    this.b2bYN = b2bYN;
  }

  /**
   * Gets the b2e yn.
   * 
   * @return the b2e yn
   */
  public String getB2eYN() {
    return b2eYN;
  }

  /**
   * Sets the b2e yn.
   * 
   * @param b2eYN the new b2e yn
   */
  public void setB2eYN(String b2eYN) {
    this.b2eYN = b2eYN;
  }

}
