/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.pojo.rentcar
 * @FileName: PriceInfo.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:12
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.b2bb2e;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 會員折扣價.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PriceInfo implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5256804374970853899L;

  /** The role. */
  private String role;
  
  /** The price. */
  private String price;

  /**
   * Gets the role.
   * 
   * @return the role
   */
  public String getRole() {
    return role;
  }

  /**
   * Sets the role.
   * 
   * @param role the new role
   */
  public void setRole(String role) {
    this.role = role;
  }

  /**
   * Gets the price.
   * 
   * @return the price
   */
  public String getPrice() {
    return price;
  }

  /**
   * Sets the price.
   * 
   * @param price the new price
   */
  public void setPrice(String price) {
    this.price = price;
  }


}
