/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.pojo
 * @FileName: CodeDetail.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo;

/**
 * The Class CodeDetail.
 * 
 * <pre>
 * 
 * </pre>
 * 
 * @author Kent
 */
public class CodeDetail {

  /** The code. */
  private String code;
  
  /** The name. */
  private String name;
  
  /** The desc. */
  private String desc;
  
  /** The type. */
  private String type;

  /**
   * Gets the code.
   * 
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * Sets the code.
   * 
   * @param code the new code
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * Gets the name.
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   * 
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Gets the desc.
   * 
   * @return the desc
   */
  public String getDesc() {
    return desc;
  }

  /**
   * Sets the desc.
   * 
   * @param desc the new desc
   */
  public void setDesc(String desc) {
    this.desc = desc;
  }

  /**
   * Gets the type.
   * 
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * Sets the type.
   * 
   * @param type the new type
   */
  public void setType(String type) {
    this.type = type;
  }

}
